# Attention

You likely don't need this unless you want to have access to the basic UNIX
commands inside the distroless image for the purposes of debugging the
container at runtime.

You **should not** be using this in production, for it's pointless to add these
additional commands in a distroless image and run it in production.

## Building and Running

Pick the desired Python version from the list of tags available
[here](https://hub.docker.com/r/ragavan/python-distroless/tags?page=1&ordering=last_updated).

```shell
# Replace the following with your Dockerhub username
username=ragavan

# Or pick your version, as mentioned above.
version=3.7.3
tag="${username}/python-distroless:debug-${version}"
docker build --pull --no-cache \
    --build-arg version=$version -t $tag \
    -f python-distroless-debug.dockerfile .
docker push $tag

# To create and run a container from the image
docker run -it $tag
```
