# Python Base image for builds

The associated [dockerfile](./python.dockerfile) and the [script](./build.sh)
together help build a Debian (buster) based Python docker image, which includes
the set of packages most commonly needed during a typical Python build process.
The goal here is to create Debian based Python base images, which could be used
in a builder pattern stage for the creation of another, say, test or runtime
container, for example.

The final image contains:

- [pyenv](https://github.com/pyenv/pyenv), a tool for managing Python versions
  on the machine.
- [Poetry](https://python-poetry.org/), a dependency management and packaging
  tool for Python.

## How to build

```bash
./build.sh <dockerhub-username> <python-version>
# For example:
# ./build.sh ragavan 3.8.0
```

Where,

- **dockerhub username**: The dockerhub username to which the image should be
  tagged with the version and pushed.
- **python version**: The specific version of Python which you need to build.
  Its source would be downloaded from [here](https://www.python.org/downloads/),
  compiled, and built into the image.

*Note*: You might want to configure your docker so it will have the permissions
to push to your docker account. The details of that are out of scope for this
document, but the procedure is described
[here](https://docs.docker.com/engine/reference/commandline/login/).
